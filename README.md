# Praktika koha leidmise App

Praktika koha leidmise App aitab Vali-IT projektis osalevatel  õpilastel leida endale praktika töökoht peale teooria osa läbimist.
Aitab luua ülevaadet, milliste ettevõtetega oled ühendust võtnud, kasutades erinevaid suhtluskanaleid, nende vastused ja protsessi kulgemine.
Motiveerib kasutajat mitte loobuma kui esimesele kontakti võtmise viisile ei ole vastatud. Kasuta vähemalt 3 kontakti võtmise viisi kui ei ole vastatud.
1. Kasutaja avab browseris lehe ja sisestab sinna oma isikuandmed ja loob kasutaja konto; eesnimi, perenimi, vanus.
2. Sisestab Ettevõtte nime, kuhu soovib praktikale minna ja valib välja sisendi, mis suhtluskanali kaudu ühendust võttis.
3. Leht näitab ettevõtete nimekirja, mis kanali kaudu on ühendust võetud ja mis on staatus
 - Sisestad ettevõtte, kuupäeva, kontaktid, CV versiooni jne.
 - Jooksvalt täidad staatust - saadetud, vastatud, proovitöö, vestlus, praktika koha saamine jne.
 - Koondis saad vaadata kokkuvõtet ja filtreerida.
 
 