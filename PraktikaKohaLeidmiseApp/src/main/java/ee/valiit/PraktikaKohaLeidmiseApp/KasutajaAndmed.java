package ee.valiit.PraktikaKohaLeidmiseApp;


public class KasutajaAndmed {

    private int id;
    private String avatar;
    private String eesnimi;
    private String perekonnanimi;
    private String vanus;

    public KasutajaAndmed() {
    }

    public KasutajaAndmed(String avatar, String eesnimi, String perekonnanimi, String vanus) {
        this.avatar = avatar;
        this.eesnimi = eesnimi;
        this.perekonnanimi = perekonnanimi;
        this.vanus = vanus;
    }

    public int getId() {
        return id;
    }

    public String getAvatar() {
        return Security.xssFix(avatar);
    }

    public String getEesnimi() {
        return Security.xssFix(eesnimi);
    }

    public String getPerekonnanimi() {
        return Security.xssFix(perekonnanimi);
    }

    public String getVanus() {
        return Security.xssFix(vanus);
    }

}

