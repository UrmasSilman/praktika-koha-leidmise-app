package ee.valiit.PraktikaKohaLeidmiseApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.*;
import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class PraktikaKohaLeidmiseAppApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PraktikaKohaLeidmiseAppApplication.class, args);
        System.out.println(" Saime tööle");
    }

    @Autowired          // Ühendab ära klassi Jdbc dependency muutuja nimega jdbc//
            JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("CONFIGURE Database Tables");

        // selleks et andmed andmebaasist ära ei kaoks + "if not exists"
        // jdbcTemplate.execute("DROP TABLE IF EXISTS kasutajaAndmed");
        jdbcTemplate.execute("CREATE TABLE if not exists kasutajaAndmed (id SERIAL, avatar TEXT, eesnimi TEXT, perekonnanimi TEXT, vanus TEXT)");

        String sqlKasutajad = "INSERT INTO kasutajaAndmed (avatar, eesnimi, perekonnanimi, vanus) VALUES ('avatar', 'eesnimi', 'perekonnanimi', 'vanus')";
        System.out.println(sqlKasutajad);
        System.out.println("Kasutaja andmed sisse loetud");


        // jdbcTemplate.execute("DROP TABLE IF EXISTS firmaAndmed");
        jdbcTemplate.execute("CREATE TABLE if not exists firmaAndmed (id SERIAL, kasutajaid TEXT, firmanimi TEXT, kontaktisikunimi TEXT, telefon TEXT, email TEXT, linkedinkontakt TEXT, kommentaar TEXT, kuupaev TEXT)");

        String sqlFirmad = "INSERT INTO firmaAndmed (kasutajaid, firmanimi, kontaktisikunimi, telefon, email, linkedinkontakt, kommentaar) VALUES ('kasutajaid', 'firmanimi', 'kontaktisikunimi', 'telefon', 'email', 'linkedinkontakt', 'kommentaar', 'kuupaev')";
        System.out.println(sqlFirmad);
        System.out.println("Firma andmed sisse loetud");


        // Email.send("admin@naidis.ee", "Registreerimise kinnitus", "<p>Tere</p> <p>Oled registreeritud PKLA kasutajaks!</p>"); // email saatmise käsk
    }
}
