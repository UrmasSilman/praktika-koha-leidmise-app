  document.querySelector('form').onsubmit = async function(event) { // "async" on selleks et töötaks "await"
	event.preventDefault()

	// console.log("submit käivitus")
	// Korjame kokku formist info

	var avatar = document.querySelector('#avatar').value
	var eesnimi = document.querySelector('#eesnimi').value
	var perekonnanimi = document.querySelector('#perekonnanimi').value
	var vanus = document.querySelector('#vanus').value

	document.querySelector('#avatar').value = ""
	document.querySelector('#eesnimi').value = ""
	document.querySelector('#perekonnanimi').value = ""
	document.querySelector('#vanus').value = ""

	// POST päring postitab uue andmetüki serverisse

	var APIurl = "http://localhost:8080/uusKasutaja/write"
	await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({avatar: avatar, eesnimi: eesnimi, perekonnanimi: perekonnanimi, vanus: vanus}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
	refreshTabel()
}
var refreshTabel = async function(){
    console.log("refreshTabel läks käima!")
    var APIurl = "http://localhost:8080/uusKasutaja/read"
    var paring = await fetch(APIurl)
    var vastus = await paring.json()
    console.log("uus kasutaja " +vastus)
    var kasutajaHTML = ""
    for(var kasutaja of vastus){
        var avatar = kasutaja.avatar
        var eesnimi = kasutaja.eesnimi
        var perekonnanimi = kasutaja.perekonnanimi
        var vanus = kasutaja.vanus
        if (avatar.length >0) {
        kasutajaHTML += "  "+"<img src='"+avatar+" ' alt='' width='100'/>"
        }
        kasutajaHTML += "  "+eesnimi+"  "+perekonnanimi+", "+vanus+"<br>"
        document.querySelector("#kasutajaSelect").innerHTML += "<option value='"+kasutaja.eesnimi+"'>"+kasutaja.eesnimi+"</option>"

    }
    document.querySelector("#uusKasutaja").innerHTML = kasutajaHTML
    //document.querySelector("#kasutajaSelect").innerHTML += "<option value='test nimi'>test dip</option>"

}
refreshTabel()

document.querySelector("#kasutajaSelect").onchange = function(event){
    var kasutaja = document.querySelector("#kasutajaSelect").value
    localStorage.setItem("praegune kasutaja", kasutaja)

}
