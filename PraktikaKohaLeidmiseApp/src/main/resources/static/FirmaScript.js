  document.querySelector('form').onsubmit = async function(event) { // "async" on selleks et töötaks "await"
	event.preventDefault()

	// console.log("submit käivitus")

	// Korjame kokku formist info

   	var kasutajaid = document.querySelector('#kasutajaid').value
   	var firmanimi = document.querySelector('#firmanimi').value
    var kontaktisikunimi = document.querySelector('#kontaktisikunimi').value
    var telefon = document.querySelector('#telefon').value
    var email = document.querySelector('#email').value
    var linkedinkontakt = document.querySelector('#linkedinkontakt').value
    var kommentaar = document.querySelector('#kommentaar').value
    var kuupaev = document.querySelector('#kuupaev').value

    document.querySelector('#kasutajaid').value = ""
    document.querySelector('#firmanimi').value = ""
    document.querySelector('#kontaktisikunimi').value = ""
    document.querySelector('#telefon').value = ""
    document.querySelector('#email').value = ""
    document.querySelector('#linkedinkontakt').value = ""
    document.querySelector('#kommentaar').value = ""
    document.querySelector('#kuupaev').value = ""

    	// POST päring postitab uue andmetüki serverisse

    var APIurl = "http://localhost:8080/uusFirma/write" // See on serveri poolt antud URL
    await fetch(APIurl, {
    	method: "POST",
    	body: JSON.stringify({kasutajaid: kasutajaid, firmanimi: firmanimi, kontaktisikunimi: kontaktisikunimi, telefon: telefon, email: email, linkedinkontakt: linkedinkontakt, kommentaar: kommentaar, kuupaev: kuupaev}),
    	headers: {
    		'Accept': 'application/json',
    		'Content-Type': 'application/json'
    	}
    })
    refreshTabel()

}
    var refreshTabel = async function(){
        console.log("refreshTabel läks käima!")
        var APIurl = "http://localhost:8080/uusFirma/read?kasutaja=" + localStorage.getItem("praegune kasutaja")
        var paring = await fetch(APIurl)
        var vastus = await paring.json()
        console.log("uus firma " + vastus)
        var firmaHTML = "<table style='width:100%'><tr><th>Kasutaja ID</th><th>Firma nimi</th> <th>Kontaktisiku nimi</th> <th>Telefon</th><th>E-mail</th>"
        firmaHTML += "<th>LinkedIn kontakt</th><th>Kuupaev</th></tr>"
        // <th>Kommentaar</th>

        var Firmanimed = []
	    for(var firma of vastus){
	        if (Firmanimed.includes(firma.firmanimi)){
	            continue;
	        }
	        Firmanimed.push(firma.firmanimi);
	        var kasutajaid = firma.kasutajaid
	        var firmanimi = firma.firmanimi
	        var kontaktisikunimi = firma.kontaktisikunimi
	        var telefon = firma.telefon
	        var email = firma.email
	        var linkedinkontakt = firma.linkedinkontakt
	        // var kommentaar = firma.kommentaar
	        var kuupaev = firma.kuupaev

	        firmaHTML += "<tr><th>"+kasutajaid+"</th><th><a href='/FirmaFilter.html?firma="+firmanimi+"'>"+firmanimi+"</a></th><th>"+kontaktisikunimi+"</th><th>"+telefon+"</th><th>"+email+"</th><th>"+linkedinkontakt+"</th><th>"+kuupaev+"</th></tr>"
            // <th>"+kommentaar+"</th>
	    }
	     firmaHTML += "</table>"
	    document.querySelector("#Firmad").innerHTML = firmaHTML
}
    refreshTabel()
